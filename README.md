**Erste Hilfe:** [Das Git Cheat Sheet](https://about.gitlab.com/images/press/git-cheat-sheet.pdf).

<hr/>

### Inhalt:

* [Einloggen auf git.thm.de](#einloggen-auf-gitthmde)
* [Erzeugen und Hinzufügen eines SSH Schlüssels](#erzeugen-und-hinzufügen-eines-ssh-schlüssels)
* [Ein neues Projekt erstellen](#ein-neues-projekt-erstellen)
* [Clonen eines Projektes](#clonen-eines-projektes)
* [Änderungen in einem Projekt veröffentlichen](#änderungen-in-einem-projekt-veröffentlichen)
* [Ein Projekt forken](#ein-projekt-forken)
* [Issues und Merge Requests aktivieren](#issues-und-merge-requests-aktivieren)
* [Verwendung einer CI Pipeline](#verwendung-einer-ci-pipeline)
* [Verwendung der Container Registry](#verwendung-der-container-registry)
* [Verwendung von GitLab Pages](#verwendung-von-gitlab-pages)
* [Hilfreiche Links](#hilfreiche-links)


# GitLab Basics

Besuchen Sie die Website git.thm.de:

![img/landingpage.png](img/landingpage.png)

## Einloggen auf git.thm.de

Drücken Sie oben rechts auf `Sign In`, um sich am zentralen Authentifizierungsdienst der THM anzumelden:

![img/cas.png](img/cas.png)

**Hinweis:** Wenn Sie einen externen Zugang verwenden, drücken Sie auf das `?`-Symbol links neben dem `Sign In`-Button. Wählen Sie `Login methods` aus. Auf der darauf folgenden Seite klappen Sie mit einem Klick auf `Sign in with external access` die Eingabemaske für den externen Login aus. Loggen Sie sich mit Ihren Daten ein.

Sie sind nun eingeloggt:

![img/logged-in.png](img/logged-in.png)

## Erzeugen und Hinzufügen eines SSH Schlüssels

Um besonders einfach und sicher mit git.thm.de zu arbeiten, sollten Sie einen SSH Schlüssel erzeugen und hinzufügen.

Erzeugen Sie einen sicheren ED25519 Schlüssel:

`ssh-keygen -t ed25519 -C <comment>`

![img/create-key.png](img/create-key.png)

Den öffentlichen Schlüssel (standardmäßig gespeichert unter `~/.ssh/id_ed25519.pub`) können Sie sich nun mit `cat ~/.ssh/id_ed25519.pub` anzeigen lassen:

![img/pubkey.png](img/pubkey.png)

Kopieren Sie den Schlüssel.

Auf git.thm.de, klicken Sie auf Ihr Profilbild oben rechts und wählen Sie `Edit profile` aus. Navigieren Sie in der Seitenleiste auf `SSH Keys` und fügen Sie den kopierten Schlüssel ein:

![img/key_gitlab.png](img/key_gitlab.png)

Mit einem Klick auf `Add key` ist nun Ihr SSH Schlüssel hinterlegt.

## Ein neues Projekt erstellen

Erstellen Sie ein neues Projekt:
* auf der Startseite durch einen Klick auf `Create a project`
* über den `Menu`-Button in der Kopfleiste
* über den `+`-Button in der Kopfleiste

Füllen Sie die Eingabemaske aus:

![img/new_project.png](img/new_project.png)

Ihr neues Projekt ist nun bereit.

## Clonen eines Projektes

Um ein Projekt zu klonen, benötigen Sie die Adresse des Projektes. Ihnen steht zur Auswahl, das Projekt mittels SSH oder HTTPS zu klonen. Drücken Sie dafür auf den Button `Clone`:

![img/before_clone.png](img/before_clone.png)

**Dringende Empfehlung:** klonen Sie Ihr Projekt mit SSH.

Kopieren Sie die Adresse aus dem Feld `Clone with SSH`. Nun führen Sie den Befehl `git clone <address>` aus, um das Projekt lokal zu klonen:

![img/clone.png](img/clone.png)

Wenn Sie das erste Mal mit git.thm.de über SSH interagieren, werden Sie aufgefordert, den Fingerprint des öffentlichen Schlüssels zu bestätigen. Diesen können Sie auf der [Konfigurationsübersicht von git.thm.de](https://git.thm.de/help/instance_configuration) überprüfen:

![img/instance_conf.png](img/instance_conf.png)

## Änderungen in einem Projekt veröffentlichen

Bevor Sie Änderungen in einem Projekt veröffentlichen, sollten Sie Ihre lokale git Installation konfigurieren. Dazu geben Sie mindestens Ihren Namen und Ihre E-Mail-Adresse an:

![img/git_conf.png](img/git_conf.png)

```
git config --global user.name <name>
git config --global user.email <email>
```

**Wichtig:** Achten Sie darauf, dass die angegeben E-Mail-Adresse mit der E-Mail-Adresse Ihres git.thm.de-Profils übereinstimmt! Eine Zuordnung Ihrer Commits zu Ihrem Profil kann ansonsten nicht stattfinden. Dies kann insbesondere bei Prüfungsleistungen problematisch sein.

Sie können nun Änderungen vornehmen und anschließend auf git.thm.de veröffentlichen. Hier eine minimale Demo:

![img/demo_commit.png](img/demo_commit.png)

Erklärung:

1. Verzeichniswechsel in das geklonte Projekt "Demo Projekt"
2. Erzeugung einer neuen Datei `Hello-World.txt` mit dem Inhalt "Hello World"
3. Abfrage des lokalen git Status, zeigt u.a. Dateien mit Änderungen und mehr
4. Hinzufügen der Datei `Hello-World.txt` zum Index
5. Erzeugung eines lokalen Commits aus den Änderungen
6. Veröffentlichung des Commits auf dem entfernen Repository

Mehr Hilfe finden Sie in der [GitLab Dokumentation](https://git.thm.de/help/topics/git/index.md).

## Ein Projekt forken

Sie können Änderungen an fremden Projekten vornehmen, indem Sie diese Forken. Das bedeutet, Sie kopieren ein Projekt, auf das Sie mindestens Lese-Zugriff haben, in Ihren eigenen Namensraum. In Ihrem eigenen Namensraum können Sie das Projekt beliebig anpassen.

Rufen Sie dazu das Projekt auf, das Sie forken möchten und klicken Sie auf den Button `Fork`:

![img/project_to_fork.png](img/project_to_fork.png)

Wählen Sie unter `Project URL` den Namensraum aus, in den Sie das Projekt Forken möchten. Sie können bei Bedarf auch den Projekt-Namen und den Projekt-Slug, also den URL-Teil, ändern.

![img/fork_settings.png](img/fork_settings.png)

Die anschließend erzeugte Kopie des Original-Projektes können Sie wie gehabt klonen und bearbeiten.

Wenn Sie Ihre Änderungen dem Original-Projekt zur Verfügung stellen möchten, können Sie - sofern Sie die Berechtigung besitzen - einen Merge Request mit Ihren eigenen Änderungen an das Original-Projekt stellen.

Angenommen, Sie haben einen Branch `demo-changes` erstellt und darin Änderungen vorgenommen. Wählen Sie in der linken Seitenleiste `Merge requests` aus und klicken Sie dann auf `New merge request`.

Wählen Sie nun als `Source branch` Ihren eigenen Branch aus und achten Sie darauf, im `Target branch` das Original-Projekt (in diesem Fall `arsnova/fork-demo`) auszuwählen.

![img/fork_mr.png](img/fork_mr.png)

Anschließend stellen Sie wie gehabt den Merge Request.

## Issues und Merge Requests aktivieren

Gehen Sie in die Einstellungen Ihres Projektes. Klicken Sie dazu in der linken Seitenleiste auf `Settings`. Öffnen Sie den Reiter `Visibility, project features, permissions`. Aktivieren Sie die Knöpfe für Issues und Merge Requests:

![img/issues_mr.png](img/issues_mr.png)

## Verwendung einer CI Pipeline

Gehen Sie in die Einstellungen Ihres Projektes. Klicken Sie dazu in der linken Seitenleiste auf `Settings`. Öffnen Sie den Reiter `Visibility, project features, permissions`. Aktivieren Sie den Knopf für CI/CD:

![img/ci_activate.png](img/ci_activate.png)

Um vorinstallierte Runner zu verwenden, klicken Sie in der Seitenleiste der Einstellungen auf `CI/CD`. Öffnen Sie den Reiter `Runners`. Aktivieren Sie nun den Knopf für shared runners:

![img/runners_activate.png](img/runners_activate.png)

Für die Verwendung einer CI-Pipeline muss eine Konfigurationsdatei `.gitlab-ci.yml` im Wurzelverzeichnis des Projektes erzeugt werden. Die Konfiguration einer CI-Pipeline liegt nicht im Rahmen der vorliegenden Dokumentation. Sie finden mehr Informationen dazu in der [Dokumentation von gitlab](https://git.thm.de/help/ci/index.md).

## Verwendung der Container Registry

Zum Aktivieren der Container Registry, gehen Sie in Ihre Projekt-Einstellungen. Aktivieren Sie unter dem Reiter `Visibility, project features, permissions` den Knopf für Container registry:

![img/activate_registry.png](img/activate_registry.png)

Mehr Informationen zur Container Registry finden Sie in der [Dokumentation von gitlab](https://git.thm.de/help/user/packages/container_registry/index.md).

Wenn Sie Container Images in Ihrer Projekt-Pipeline bauen möchten, verwenden Sie am besten Kaniko: [https://github.com/GoogleContainerTools/kaniko](https://github.com/GoogleContainerTools/kaniko). Ein entsprechender Runner mit dem Tag `kaniko` ist als Shared Runner eingerichtet.

## Verwendung von GitLab Pages

Mit GitLab Pages können Sie eine statische Website direkt aus Ihrem Projekt heraus bereitstellen.

Für die Verwendung von GitLab Pages benötigen Sie eine funktionierende CI Pipeline (s. oben).

In Ihrer CI Konfiguration, erstellen Sie einen Job mit dem Namen "pages". Dieser Job **muss** ein Artefakt mit dem Namen `public` erzeugen, in dem sich die statischen Dateien für die Website befinden, inkl. einer `index.html` Datei.

Um den Einstieg zu erleichtern, sind auf git.thm.de einige Vorlagen für GitLab Pages hinterlegt. Um diese zu verwenden, erstellen Sie einfach ein neues Projekt, wählen Sie `Create from template` aus und verwenden Sie die Vorlage Ihrer Wahl.

Die GitLab Pages Domain von git.thm.de lautet git-pages.thm.de. Das THM GitLab unterstützt zudem die Angabe von beliebigen Domains. Die IP Adresse lautet `212.201.6.254`. Nach einer Verifikation kann für jede benutzerdefinierte Domain automatisch ein SSL-Zertifikat von Let's Encrypt erzeugt werden.

Sie können auch Gruppen- und User-Pages erstellen, indem Sie einem Projekt einen bestimmten Namen geben:

![img/pages_special.png](img/pages_special.png)

GitLab Pages bietet umfangreiche Funktionen, die nicht im Rahmen der vorliegenden Dokumentation liegen. Lesen Sie mehr darüber in der [GitLab Dokumentation](https://git.thm.de/help/user/project/pages/index.md).

Wenn Sie alles eingerichtet haben, können Sie in den Projekteinstellungen unter dem Link `Pages` den Link zu Ihrer Seite sehen:

![img/pages.png](img/pages.png)

Sie sollten von der Einstellung `Force HTTPS` Gebrauch machen.

## Hilfreiche Links

Sollten Sie weiterhin Hilfe brauchen oder möchten sich mit dem Funktionsumfang von GitLab vertraut machen, finden Sie hier wichtige Links.

[Hilfe auf git.thm.de](https://git.thm.de/help)

[Konfigurationsübersicht von git.thm.de](https://git.thm.de/help/instance_configuration)

[GitLab User Dokumentation](https://docs.gitlab.com/ee/user/)

[GitLab Forum](https://forum.gitlab.com/)

[E-Mail an die Betreiber von git.thm.de](mailto:scm-admin@lists.thm.de)
